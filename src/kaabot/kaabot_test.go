package main

import (
	"log"
	"math/rand"
	"testing"
)

func TestGetRandQuote(t *testing.T) {
	quotes := []string{"quote1", "quote2", "quote3"}

	if GetRandQuote(quotes, GetRandIndice(quotes)) == "" {
		t.Error("I should get a random quote but i got an empty string !")
	}
}

func TestSuccessiveGetRandQuote(t *testing.T) {
	quotes := []string{"quote1", "quote2", "quote3"}

	if GetRandQuote(quotes, GetRandIndice(quotes)) == GetRandQuote(quotes, GetRandIndice(quotes)) {
		t.Error("I should get 2 differents quotes here")
	}
}

func TestRandominess(t *testing.T) {
	var errc int
	for i := 0; i < 6; i++ {
		if !IsRandomSimple() {
			errc++
		}
	}

	if errc > 1 {
		t.Errorf("I should get more randominess than that (i think) %v", errc)
	}
}

func IsRandomSimple() bool {
	var rand1 = 10 * rand.Float32()
	var rand2 = 10 * rand.Float32()

	log.Printf("%v - %v", rand1, rand2)

	if rand1 == rand2 {
		return false
	}

	if rand2 == (rand1 + 1) {
		return false
	}

	return true
}
