#!/bin/bash

set -eo pipefail

function redis-local-start() {
    echo -n "+ starting redis : "
    redis-server &
    until $(redis-cli -h "${KAABOT_REDIS_HOST}" -p "${KAABOT_REDIS_PORT}" -a "${KAABOT_REDIS_PASSWORD}" -n "${KAABOT_REDIS_DB}" ping > /dev/null); do
        echo "."
        sleep 1
    done
    echo "OK"
    return 0
}

function redis-populate() {
    echo -n "+ loading default data into ${KAABOT_REDIS_HOST} : "
    DATA=$(cat /data/kaamelot.json)
    redis-cli -h "${KAABOT_REDIS_HOST}" -p "${KAABOT_REDIS_PORT}" -a "${KAABOT_REDIS_PASSWORD}" -n "${KAABOT_REDIS_DB}" set kaaquotes "${DATA}"
    echo "OK"
    return 0
}

function kaabot-start() {
    echo "+ starting kaabot"
    /go/bin/kaabot
}

# main
if [[ "${KAABOT_REDIS_HOST}" == "localhost" ]]; then
    redis-local-start
fi

redis-populate
kaabot-start