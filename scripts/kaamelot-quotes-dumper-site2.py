#!env python

from lxml import html, etree
import simplejson as json
import requests

from pprint import pprint

url = 'http://kaamelott.over-blog.fr/pages/Repliques_cultes-2024869.html'
hparser = etree.HTMLParser(encoding='utf-8')
tree   = etree.parse(requests.get(url).text, hparser)

# This will create a list of quotes
quotes = tree.xpath('//span[@class="citation"]/text()')
#print("- quotes: ")
#pprint(quotes, indent=2, width=80, depth=5)

#print(json.dumps(quotes, ensure_ascii=True, indent=4))